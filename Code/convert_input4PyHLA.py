'''
Author: Anh-Vu Mai-Nguyen
Created Date: 06/11/2021
Last Modified: 06/11//2021
Description: 
Reviewed by: ...
'''
import logging.config
import yaml

with open('./configs/convert_input4PyHLA_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')#getLogger(__name__)

from optparse import OptionParser
import os
import pandas as pd

parser = OptionParser()
parser.add_option("-d","--data_folder",type=str, 
                    default='../data',
                    help="folder contains all needed data")
parser.add_option("-i","--input_file",type=str, 
                    default='SCARS_Pilot_full.HISAT_result.csv',
                    help="Get tab file")
parser.add_option("--output_path",type=str, 
                    default='../data/input',
                    help="output folder path")
parser.add_option("--output_file",type=str, 
                    default=None,
                    help="")
(options, args) = parser.parse_args()

def convert_value(df):
    for col_name in df.columns:
        if col_name == '':
            continue
        df[col_name] = col_name.split('_')[0]+'*'+df[col_name]
    df.reset_index(inplace=True)

def save_df(df):
    if options.output_file is None:
        file_name = options.input_file.replace('.csv','_transformed.txt')
        options.output_file = os.path.join(options.output_path,file_name)
    df.to_csv(options.output_file,sep=' ',header=False)

def main():
    file_path = os.path.join(options.data_folder,options.input_file)
    df = pd.read_csv(file_path,index_col=0)
    convert_value(df)
    save_df(df)

if __name__ == "__main__":
    main()