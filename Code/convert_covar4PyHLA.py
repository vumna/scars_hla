'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/08/2021
Last Modified: 11/08/2021
Description: 
Reviewed by: ...
'''
import logging.config
import yaml

with open('./configs/convert_covar4PyHLA_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')#getLogger(__name__)

from optparse import OptionParser
import os
import pandas as pd

parser = OptionParser()
parser.add_option("-d","--data_folder",type=str, 
                    default='../data',
                    help="folder contains all needed data")
parser.add_option("--demographic",type=str, 
                    default='DGV4VN_1028.HISAT_result_demographic.csv',
                    help="Get csv file")
parser.add_option("--exposure",type=str, 
                    default='DGV4VN_1028.HISAT_result_exposure.csv',
                    help="Get csv file")
parser.add_option("--family_history",type=str, 
                    default='DGV4VN_1028.HISAT_result_family_history.csv',
                    help="Get csv file")
parser.add_option("--output_path",type=str, 
                    default='../data/input',
                    help="output folder path")
parser.add_option("--output_file",type=str, 
                    default=None,
                    help="")
(options, args) = parser.parse_args()



def read_input(path,index_col=None):
    df = pd.read_csv(path,index_col=index_col)
    return df

def read_demographic(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df

def read_exposure(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df

def read_family_history(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df

def save_df(df):
    if options.output_file is None:
        file_name = options.demographic.replace('.csv','_covar.txt')
        options.output_file = os.path.join(options.output_path,file_name)
    df.to_csv(options.output_file,sep=' ',index=False)

def main():
    df_demographic = read_demographic(os.path.join(options.data_folder,options.demographic))
    print(df_demographic.head())
    df_exposure = read_exposure(os.path.join(options.data_folder,options.exposure))
    print(df_exposure.head())
    df_family_history = read_family_history(os.path.join(options.data_folder,options.family_history))
    df_join = df_demographic.join(df_exposure).join(df_family_history)
    df_join.reset_index(inplace=True)
    df_join.rename(columns={'submitter_id':'IID'},inplace=True)
    df_join.dropna(inplace=True)
    print(df_join)
    save_df(df_join)

if __name__ == "__main__":
    main()