'''
Author: Anh-Vu Mai-Nguyen
Created Date: 06/11/2021
Last Modified: 06/11//2021
Description: 
Reviewed by: ...
'''
import logging.config
import yaml

with open('./configs/merge_allel_others_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')#getLogger(__name__)

from optparse import OptionParser
import os
import pandas as pd

parser = OptionParser()
parser.add_option("-d","--data_folder",type=str, 
                    default='../data',
                    help="folder contains all needed data")
parser.add_option("--allel",type=str, 
                    default='DGV4VN_1028.HISAT_result_allels.csv',
                    help="Get csv file")
parser.add_option("--demographic",type=str, 
                    default='DGV4VN_1028.HISAT_result_demographic.csv',
                    help="Get csv file")
parser.add_option("--exposure",type=str, 
                    default='DGV4VN_1028.HISAT_result_exposure.csv',
                    help="Get csv file")
parser.add_option("--family_history",type=str, 
                    default='DGV4VN_1028.HISAT_result_family_history.csv',
                    help="Get csv file")
parser.add_option("--phenotype_column",type=str,
                    default='diastolic_blood_pressure',
                    help="The name of column is set as phenotype")
parser.add_option("--output_path",type=str, 
                    default='../data/input',
                    help="output folder path")
parser.add_option("--output_file",type=str, 
                    default=None,
                    help="")
(options, args) = parser.parse_args()

def read_input(path,index_col=None):
    df = pd.read_csv(path,index_col=index_col)
    return df

def read_allel(path):
    df = read_input(path,index_col=0)
    return df

def read_demographic(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df

def read_exposure(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df

def read_family_history(path):
    df = read_input(path)
    df.set_index('submitter_id',inplace=True)
    return df


def convert_value(df):
    for col_name in df.columns:
        if col_name == '':
            continue
        df[col_name] = col_name.split('_')[0]+'*'+df[col_name]

def save_df(df):
    if options.output_file is None:
        file_name = options.allel.replace('.csv','_transformed.txt')
        options.output_file = os.path.join(options.output_path,file_name)
    df.to_csv(options.output_file,sep=' ',header=False)


def main():
    df_allel = read_allel(os.path.join(options.data_folder,options.allel))
    convert_value(df_allel)
    print(df_allel.head())
    df_demographic = read_demographic(os.path.join(options.data_folder,options.demographic))
    print(df_demographic.head())
    df_exposure = read_exposure(os.path.join(options.data_folder,options.exposure))
    print(df_exposure.head())
    df_family_history = read_family_history(os.path.join(options.data_folder,options.family_history))
    print(df_family_history.head())
    df_merge = df_allel.join(df_demographic,how='left').join(df_exposure,how='left').join(df_family_history,how='left')
    df_merge.insert(0, 'phenotype', df_merge[options.phenotype_column])
    df_merge.drop(options.phenotype_column, axis=1,inplace=True)
    df_merge = df_merge['phenotype,A_1,A_2,B_1,B_2,C_1,C_2,DRB1_1,DRB1_2,DQB1_1,DQB1_2,DQA1_1,DQA1_2,DPB1_1,DPB1_2'.split(',')].dropna()
    print(df_merge.head())
    save_df(df_merge)

if __name__ == '__main__':
    main()